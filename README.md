# IUT-PROJETS ROBOTIQUE

Bienvenue sur cette page de ressources pour les projets robotique de l'IUT de Sénart-Fontainebleau.
Vous y trouverez un [dépot de fichiers](https://gitlab.com/hazaa/iut-projets/tree/master), dont les répertoires ont le contenu suivant:

*   code: des programmes dans différents langages (python,C,...) pour piloter les micro-contrôleurs et autres appareils utilisés (raspberry munie de caméra, moteurs,...)
*   docs: documents pédagogiques (sujets de TP, modèles de rapports, etc...) et [notices techniques répertoire](https://gitlab.com/hazaa/iut-projets/tree/master/docs)

Vous pourrez aussi consulter les pages des projets sur Eprel: [projet ER2](http://eprel.u-pec.fr/eprel/claroline/course/index.php?cid=5804), [projet ER3](http://eprel.u-pec.fr/eprel/claroline/course/index.php?cid=7603), et module complémentaire [MCTR](http://eprel.u-pec.fr/eprel/claroline/course/index.php?cid=4513).

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!

# Projet ER2 1ère année

Les sujets 2016-2017:

*   groupe B2: alimentation ATX 
     *    sujets pré-étude: [pdf](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_ALIMENTATION_ATX_1page_v1.pdf), [odt](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_ALIMENTATION_ATX_1page_v1.odt)
     *    [documents techniques](docs/alimentation_atx.md)
*   groupe C2: bras robot MeArm
    *    sujets pré-étude:[pdf](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_MEARM_1page_v1.pdf), [odt](https://gitlab.com/hazaa/iut-projets/tree/master/docs/2017_ER2_MEARM_1page_v1.odt)  
	*    [documents techniques](docs/me_arm.md)


MCTR
------

Parcours SIRO : Système Informatique embarqué et RObotique mobile.
Concevoir, commander et évaluer un système embarqué.

Les sujets 2016-2017:

*   révisions [arduino](http://eprel.u-pec.fr/eprel//nzltnwj/7603/document/2016_2017/co/activ_appren_arduino_motor.html), python: [cours](docs/python_diapo_v3.odp), [exos](http://eprel.u-pec.fr/eprel//nzltnwj/4513/document/2015_2016/co/activ_appren_python.html).    
*   asservissement de moteur à courant continu avec [encodeur à quadrature](docs/moteur_cc_encodeur_quadrature.md)
*   [tracking visuel](docs/tracking_visuel.md) avec tourelle.
*   interface [web](docs/interface-web.md)  

*   robot MeArm
    *     réalisation, avec plusieurs moteurs    
    *     pick and place visuel    
    *     GUI
*   wifi avec l'[ESP8266](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/network_tcp.html)
 


# FAQ

*   sous python, il y a une erreur avec "import serial": le paquet pyserial n'est pas installé sur votre pc. Il faut:
    *   télécharger [pyserial-2.6](https://pypi.python.org/pypi/pyserial/2.6)     
    *   le décompacter dans un répertoire, par exemple Bureau/data_etudiant/python 
    *   au début de chaque programme python qui utilise serial, ajouter: import sys; sys.path.append('/home/etudiant/Bureau/data_etudiant/python')
