

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!


La plupart des systèmes embarqués n'ont pas d'écran. Malgré cela il est nécessaire de les doter d'une interface utilisateur.
L'objectif de ce TP est d'arriver au résultat suivant:

*    Un serveur web va tourner sur la raspberry, qui sera la seule machine branchée à l'arduino.
*    Quand un client situé sur une autre machine se connectera sur le serveur, celui-ci communiquera en liaison série avec l'arduino.

Grâce à ce système on commandera la position d'robot mobile muni d'une tourelle réalisant un tracking visuel automatique.


Interface web pour commander des servos en position
------

Réalisons une interface web pour déplacer des servos. 


>    **Question**: pour illustrer le principe, on va lancer un serveur http sur le PC de bureau, avec la commande :
>
>        python -m SimpleHTTPServer
>         
>	 Puis on se place sur une autre machine de la salle (on dit "côté client"), on ouvre un navigateur, et on tape dans la barre de navigation l'IP de la machine où on vient de lancer le serveur. Que se passe-t-il ?
>    (pour obtenir l'IP d'une machine de la salle de TP, ouvrir un terminal et taper /sbin/ifconfig)

Maintenant qu'on a compris le principe, on va créer un serveur, qui lance un script python côté serveur quand on clique sur un bouton côté client.
Pour cela, télécharger tous les fichiers du répertoire [UI](https://gitlab.com/hazaa/iut-projets/tree/master/code/UI) sur votre machine de bureau en respectant la structure des répertoires, puis ouvrez un terminal et tapez:

     cd cgi-bin
     chmod u+x cgi-script.py

>    **Question**: ouvrez un terminal, placez-vous dans le répertoire UI, lancez le serveur:
>        python -m CGIHTTPServer
>     Puis, depuis une autre machine, connectez-vous au serveur que nous venons de lancer.
>     Dès qu'on touche au slider côté client, le script /cgi-bin/cgi-script.py s'exécute côté serveur
>     et lui transmet la nouvelle position du slider. 


>    **Question**: modifiez le script /cgi-bin/cgi-script.py, pour qu'il écrive dans un fichier la position du slider.


On va maintenant pouvoir commander un servo grâce à cette interface web. 
Branchez l'arduino sur la machine de bureau, et les servos sur l'arduino.

>    **Question**: modifiez le script /cgi-bin/cgi-script.py , remplacez l'écriture dans un fichier par une communication série ver un servo. 

>    **Question**:idem pour 2 servos (il faut changer index.html, mais aussi cgi-scripts.py)


Interface web pour contrôler des moteurs en vitesse
------

Dans cette partie, on veut contrôler des moteurs qui serviront à déplacer un robot mobile.
Ils seront commandés en vitesse, et pas en position. 

>    **Question**: commandez les moteurs en vitesse, à partir de la raspberry, sans interface web.

>    **Question**: ajouter une interface web pour commander ces moteurs en vitesse.
>     On pourra choisir un plugin jQuery. Exemple: [nipplejs](http://yoannmoinet.github.io/nipplejs/)

>    **Question**: assemblez les moteurs sur le robot et testez que tout fonctionne bien.


Raspberry Pi: robot mobile + tracking visuel
------

Dans cette partie on rassemble toutes les fonctions qu'on a programmées jusqu'ici:

*    la commande du robot mobile grâce à l'interface web.
*    le tracking visuel automatique.


>    **Question**: implémenter l'interface des moteurs commandés en vitesse depuis la raspberry.

>    **Question**: ajouter le tracking visuel automatique

>    **Question**: testez que l'ensemble fonctionne bien.


BONUS: interface web pour paramétrer le tracking visuel
------

Maintenant qu'on a compris le principe d'une interface web, on va en programmer une qui nous permettra de régler le tracking visuel.

Fonctions:

*    affichage image on/off
*    résolution x,y
*    couleur de l'objet
*    communication: print, circle, serial, zmq

Exemples: 

*    [exemple1](http://stackoverflow.com/questions/18226565/how-to-pass-values-to-python-from-a-jquerymobile-slider), [exemple 2](http://stackoverflow.com/questions/14996414/running-python-cgi-scripts-from-javascript-and-jquery-mobile-ui)


Extensions 
------

*     serveur tornado (il y a un worker qui gère le flux de données échangés avec l'interface série) ou minitornado (fait partie de pyzmq)
*     micropython:
      *    simple http server [doc](https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/network_tcp.html?highlight=http#simple-http-server), [forum1](https://forum.micropython.org/viewtopic.php?t=1033), [forum2 POST](https://forum.micropython.org/viewtopic.php?t=2793), nécessaire pour CGI.
