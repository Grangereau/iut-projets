# Documents techniques

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!


*   robotique:
    *    pince [openbionics](http://openbionics.org/)
    *    bras [MeArm](http://mime.co.uk)
*   logiciels:
    *   conception de circuits électriques: [fritzing](http://fritzing.org), kicad, proteus
*   moteurs:
    *   moteur à courant continu [digilent 6V](http://store.digilentinc.com/dc-motor-gearbox-1-53-gear-ratio-custom-6v-motor-designed-for-digilent-robot-kits/)
    *   servomoteur standard [HS-322](https://www.servocity.com/hs-322hd-servo)
    *   moteur pas-à-pas
*   composants:
    *   pont en H [PmodHB5](https://reference.digilentinc.com/reference/pmod/pmodhb5/start)
    *   carte breakout [L298N](https://github.com/yohendry/arduino_L298N)
*   programmation:
    *   arduino:    
        *    varspeed
        *    Accelstepper 
    *   raspberry pi:
        *    communication série [FTDI](https://wolfpaulus.com/embedded/raspberry_serial/), [sparkfun](https://www.sparkfun.com/products/9873)
        *    caméra: le [module](https://www.raspberrypi.org/products/camera-module/)
        *    librairie de traitement d'images [openCV](http://opencv.org)
