Bras robot 
======

Design bras
------

*    [MeArm v2](https://github.com/mimeindustries/MeArm/tree/v2)
    *   télécharger le fichier dxf
    *   l'ouvrir avec inkscape. NE PAS utiliser le redimensionnement automatique à la page A4.
    *   vérifier que les unités sont "mm" (voir les informations sur inkscape données ci-dessous)
    *   dimension des pièces: sélectionner la pièce, vous voyez s'afficher ses dimensions en mm dans la barre du haut (L et H)
*    [bibliothèque de formes](notices_techniques/MeArm/library-pattern-cutouts.svg) pour découpe laser, avec servos (normal et mini), arduino, etc...    
*    [assemblage](https://youtu.be/az8JMvvetyU)

Fournitures
------

voir [hackaday.io](https://hackaday.io/project/16537-mearm-raspberry-pi-edition) pour une liste complète de fournitures.

*    matériau de l'armature 
    *    dimension: 3mm x 212 mm x 373 mm    
    *    pour le prototype: carton (dur type calendrier) de récupération
    *    pour le produit fini: [PMMA](https://en.wikipedia.org/wiki/Poly%28methyl_methacrylate%29) __coulé__ 3mm (nom commercial: perspex,plexiglass). Attention, le PMMA __extrudé__ risque de se casser.
*    visserie: disponible à l'IUT
*    élastiques x 4
*    carte PCB [servo](https://github.com/mimeindustries/mearm-base-pcb) au format [kicad](http://kicad-pcb.org). A remplacer par l'empreinte [ServoPCB](notices_techniques/MeArm/ServoPCB-Edge.Cuts.svg) 
    *    extension: réaliser la carte avec la fraiseuse de table CIF de l'IUT ou du fablab.


Nous disposons d'un nombre limité de servos, et ils ne sont pas du même type que ceux employés par MeArm. 
Il faudra donc adapter le design.

*    nombre de micro-servos         ?
*    nombre de servos               ?
*    nombre de moteurs PmodHB5      ?

Il faut piloter ces servos. Nous utiliserons des cartes arduino, disponibles à l'IUT.
 


Logiciels de CAD (computer assisted design) 2D et 3D
------

Nous utliserons surtout le logiciel libre de dessin vectoriel 2D inkscape, 
qui est installé à l'IUT, et que vous pourrez installer chez vous.

*    2D: [inkscape](http://inkscape.org), [installation windows](https://fr.flossmanuals.net/inkscape/installation-sous-windows/), [video prise en main](https://youtu.be/7iujdCtF8cE), [guide](http://fr.flossmanuals.net/inkscape/) en français.
    *    toute mettre dans l'unité "mm" dans Fichiers>Propriétés du document>Unités par défaut , et Unités
    *    vérifier que tous les contours sont fermés (c'est nécessaire pour la découpe laser). Voir la vidéo pour savoir comment faire.
*    3D: [freecad](http://www.freecadweb.org/) 3D, permet de faire l'assemblage avec le module Assembly2. 



 
Questions/Réponses
------



| Q | R | 
| -------- | -------- | 
| "Comment modifier le design du robot sans connaître ses dimensions ? Concernant les dimensions de l'armature du robot, nous avons trouvé une trame avec toutes les figures géométriques qui le composent mais sans les dimensions." | vous disposez du fichier dxf de l'armature du robot. Pour avoir les dimensions, regardez la rubrique "Design bras" ci-dessus. |
| Comment modifier le design du robot sans connaître les dimensions du servomoteur et du MCC ?  | le [dessin technique du servo HS322HD](notices_techniques/HS322HD.pdf). Le [dessin technique du MCC](notices_techniques/moteur_DC_ig220053x00085r_ds.pdf) se trouve p.5 de la notice technique. | 
| Serait-il possible de pouvoir emprunter un servomoteur HS-322 ainsi qu'un MCC pour pouvoir réaliser le prototype ? | oui, contactez-moi pour que nous prenions rendez-vous. |
| Pouvez vous nous confirmer que l’on puisse se mettre à 3 ou faut il que quelqu'un reste tous seul  ? | s'il y a un seul groupe de 3, c'est bon. |
| Pour les modifications du bras robotique, doit on découper toutes les pièces du bras ou juste les pièces que l’on a besoin de modifier ? | vous aurez besoin de toutes les pièces pour assembler votre robot. |
| Est ce à nous de choisir le matériau et de le ramener au Fablab pour la découpe ? | lisez la rubrique Fournitures ci-dessus. Pour le prototypage vous pourrez travailler avec du carton dense 3mm que vous récupérerez. Une fois que nous serons sûrs du résultat, nous passerons à un autre matériau (contreplaqué ou plastique). Si vous disposez de chutes de ces matériaux, vous pouvez les tester. |
|En identifiant les pièces du robot sur le fichier MeArm.dxf avec inkscape, je ne parviens pas à trouver cette pièce : Dans le montage elle sert à monter le micro-servo pour faire tourner le bras. Faut-il créer cette pièce ? ![MeArmParts](notices_techniques/MeArm/MeArm2_parts.png) | Cette pièce est une carte électronique. Pour le prototypage nous pouvons la remplacer par une pièce d'acrylique de même forme: [ServoPCB](notices_techniques/MeArm/ServoPCB-Edge.Cuts.svg) . Attention toutfois à l'épaisseur. |
|Faut-il aussi modifier le bras pour les dimensions de l’Arduino UNO ? | Ajoutez 3 trous pour l'arduino, en utlisant la bibliothèque de formes mentionnée dans la partie "Design". |
|Avec les micro-servos originaux, les embouts ont une certaine forme: Alors que dans la fiche technique des HS-322HD ils sont ronds : Faut-il prendre en compte la forme des embouts ou ils seront les mêmes que les originaux ? | Je n'ai pas encore de réponse. Je fais l'inventaire des embouts disponibles, et vous réponds ensuite.|


Liens
------

*    [kickstarter](https://www.kickstarter.com/projects/mime/mearm-pi-build-your-own-raspberry-pi-powered-robot)
