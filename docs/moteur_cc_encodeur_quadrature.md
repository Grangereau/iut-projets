Asservissement de moteur à courant continu avec encodeur à quadrature
======

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!


Nous souhaitons réaliser l'asservissement en position d'un moteur à courant continu. 
Pour cela nous utiliserons:
*    un moteur à courant continu [IG220053](notices_techniques/moteur_DC_ig220053x00085r_ds.pdf) de tension d'alimentation 6V et de courant max 200mA
*    un pont en H qui se trouve sur la carte [PmodHB5](notices_techniques/PmodHB5_RevD_rm.pdf)
*    un encodeur à quadrature composé de deux détecteurs à effet Hall, et d'un aimant.
*    nous programmerons le tout sur un arduino uno.


>   **Question**: faites les tp de révisions arduino et python.

Puis lisez entièrement les documents techniques avant de passer à la suite.

>   **Question**:  vérifiez que vous savez ce qu'est un pont en H et comment il fonctionne. (voir cours autres matières)

>   **Question**: cabler le PmodHB5 sur l'arduino, en placant les capteurs à effet hall sur les pins 2 et 3; le pin DIR sur 8 et le pin ENABLE sur le pin 9. Demander à l'enseignant avant d'alimenter.


Attention: 
*    l'alimentation du moteur doit etre séparée du reste. Utiliser une alimentation CC que vous brancherez sur le bornier bleu de la carte PmodHB5.
*    ne pas changer de direction alors que la vitesse est non nulle, cela peut endommager le pont en H. Laisser d'abord la vitesse à zero pendans 5 à 10ms avant de changer de direction.


## Compte-tour simple
Le moteur n'est pas alimenté dans cette partie.
Vous avez cablé les capteurs magnétiques sur les pins 2 et 3 de l'arduino. Nous allons lire l'état de ces pins.

>   **Question**: utilisez le programme [position_control_encoder_pmodhb5_simple.ino](../code/position_control_encoder_pmodhb5_simple/position_control_encoder_pmodhb5_simple.ino) pour lire sur le console série les informations qui viennent de l'arduino. Que se passe-t-il quand vous faites tourner à la main l'aimant situé sur l'axe du moteur ?

Lisez [vhdl], surtout la partie "codeurs incrémentaux" pour comprendre ce qui se passe.
Comptez le nombre d'incréments par tour de l'arbre moteur. Comme le réducteur a un rapport 1/53 (voir la doc du moteur), on obtient 318 incréments par tour en sortie du réducteur.


## Compte-tour simple avec interruptions
Le moteur n'est pas alimenté dans cette partie.

Nous allons utiliser le mécanisme des [interruptions](https://fr.wikipedia.org/wiki/Interruption_%28informatique%29) dans cette partie.

>   **Question**: utilisez le programme [position_control_encoder_pmodhb5_interrupt.ino](../code/position_control_encoder_pmodhb5_simple/position_control_encoder_pmodhb5_interrupt.ino), modifiez-le pour voir apparaître sur la console série l'état actuel du compteur, par exemple toutes les 100ms 

>   **Question**: Comptez le nombre d'incréments par tour de l'arbre moteur. Retrouve-t-on le même chiffre que ci-dessus ?


Attention: ne rien mettre de superflu dans les interruptions. Par exemple, ne pas y mettre de Serial.print()


## Compte-tour double 
Le moteur n'est pas alimenté dans cette partie.

Répeter ce que vous avez fait ci-dessus, mais cette fois en utilisant deux interruptions, une pour chaque encodeur, comme dans cet [exemple](http://playground.arduino.cc/Main/RotaryEncoders#Example3)

## Commande du moteur à courant continu avec le pont en H
On va alimenter le moteur dans cette partie.

La commande du pont en H sera effectué à l'aide d'un signal PWM, dont le rapport cyclique est variable entre 0 (0 %) et 255 (100 %).
Pour cela on va utiliser les commandes:

    #define analogOutPin 9  // à mettre au début
    analogWrite(analogOutPin, x);  // à mettre dans loop()

Commande en vitesse avec un potentiomètre, en boucle ouverte
------

On a vu ci-dessus comment commander en vitesse le pont en H avec un signal pwm.

Maintenant on veut lire la tension aux bornes d'un potentiomètre pour commander le moteur en vitesse. 
On lit l'état du pin analogique A0. Quand on lui applique une tension comprise entre 0 et 5v, le pin renvoie une valeur entière située entre 0 (pour 0V) et 1023 (pour 5V).
Enfin, on transforme cet entier en une valeur comprise entre 0 et 255 pour commander le PWM qui commande le pont en H.

>    **Question**: cabler le potentiomètre sur le pin A0, et commander le moteur en vitesse comme expliqué ci-dessus.


Astuce: utiliser la fonction arduino [map](https://www.arduino.cc/en/Reference/Map)


Remarque: on n'a pas utilisé les encodeurs dans cette partie, on est en boucle ouverte.



Commande en position avec vitesse positive
------

Choisir une position cible pos_target (par exemple un ou deux tours complets du moteur, côté réducteur).
Soit l'algorithme suivant:

```C
while(True){
	pos=lire_position();
	if(pos<pos_target)
		speed = s;
	else
		speed = 0;	
}
```

>    **Question**: programmer cet algorithme. Avec le moniteur série, affichez la position actuelle. Constatez-vous un dépassement ? Quel est le problème avec cet algorithme ?


Asservissement en position avec retour en arrière
------

Dans la question précédente, on a vu qu'il y avait un dépassement et qu'on ne pouvait pas revenir en arrière.

On veut revenir en arrière pour corriger le dépassement.

Pour cela il faudra changer la valeur du pin DIR, avec la commande digitalWrite()

>    **Question**: proposer un algorithme qui permette de corriger le dépassement. Utiliser des intervalles

>    **BONUS** (à faire à la fin): peut-on utiliser un correcteur PID ?


Asservissement en position avec un potentiomètre
------
Lire les valeurs du potentiomètre sur le port analogique A0 (compris entre 0 et 1024).

Transformer cette vaaleur en une position cible pour le compteur.
Astuce: utiliser la fonction arduino [map](https://www.arduino.cc/en/Reference/Map)

Commander la position du moteur à l'aide de cette valeur


Asservissement en position depuis un PC
------

Commander le moteur en position:
*    depuis python. envoyer un octet. puis saisie au clavier.
*    avec une interface graphique: Tkinter, ou matplotlib.widget, si disponible
*    récupérer sur le port série la valeur de consigne et la valeur effective du compteur, tracer un graphe avec votre interface graphique.

Caractérisation du servo 
------
Le système que nous avons conçu s'apparente à un servomoteur (ou servo), qu'on peut modéliser comme un sytème du second ordre.
Nous nous interessons ici aux performances du servo du point de vue de la théorie des asservissements.

*	 performance, modélisation sous forme de second ordre
*    comparaison avec dcservo, avec dynamixel.

Liens:

*    commande en couple et en intensité ?
*    identification : ?
*    video [dcservo4](https://www.youtube.com/watch?v=rG9CGnvapT8), [dcservo5](https://www.youtube.com/watch?v=JVAEMJifVaM)
*    cours [matlab](http://www.eng.buffalo.edu/~ms329/ProjectPages/MAE334_DynControlsMotorLab.pdf), [c](http://me.umn.edu/courses/me4231/labs/Lab6.pdf), 
*    avec raspberry directement [forum](https://www.raspberrypi.org/forums/viewtopic.php?f=37&t=70758), librairie [pigio](http://abyz.co.uk/rpi/pigpio/examples.html)

Asservissement en position avec interface web
------

*    interface web :
	*    avec python + jquery [exemple1](http://stackoverflow.com/questions/18226565/how-to-pass-values-to-python-from-a-jquerymobile-slider), [exemple 2](http://stackoverflow.com/questions/14996414/running-python-cgi-scripts-from-javascript-and-jquery-mobile-ui)
	*    (serveur web python [exemple](http://apprendre-python.com/page-python-serveur-web-creer-rapidement) )
	*    esp httpd https://github.com/mathew-hall/esp8266-dht http://harizanov.com/2015/03/diy-internet-connected-smart-humidifier/ https://github.com/mathew-hall/esp8266-dht	
	*    tornado http://archive.fabacademy.org/archives/2016/doc/WebSocketConsole.html http://www.instructables.com/id/Use-ESP8266-to-Internet-enabled-AC-Appliances/?ALLSTEPS
*    interface web embarquée :
    *    wifi: esp8266: transposer le cas python+jquery avec micropython.  (exemple [rgb led lua](http://randomnerdtutorials.com/10-diy-wifi-rgb-led-mood-light-with-esp8266-step-by-step/) ) 
    *    arduino yun http://www.fabiobiondi.com/blog/2014/02/html5-arduino-yun-and-angularjs-build-a-mobile-servo-controller/ http://www.tecnomacs.com/2016/12/03/lets-do-a-simple-webserver-ui-for-arduino-yun-using-bootstrap/
    *    raspberry pi + écran LCD (tactile ?)
    

LIENS
------

*     programmation: http://playground.arduino.cc/Main/RotaryEncoders , http://forum.arduino.cc/index.php?topic=355245.0  (interruptions)
*     théorie: "sampled-data feedback systems" (willsky 11.2.4)
*     asservissement en vitesse et position avec arduino et pmodhb5 sous [scilab](https://scilab.developpez.com/tutoriels/apprendre-xcos/#LIV-B-3-c)
*     asservissement en courant ? cf [forum hd17](http://hackaday.com/2015/04/20/driving-a-brushless-dc-motor-sloooooooowly/)
*     conception de circuit (avec pont en H L298N): fritzing + L298 + magnetic/optical encoder
*     PID+dcservo [misan](https://github.com/misan/dcservo)
[vhdl]:https://fr.wikiversity.org/wiki/Very_High_Speed_Integrated_Circuit_Hardware_Description_Language/Commande_de_robot_mobile_et_p%C3%A9riph%C3%A9riques_associ%C3%A9s#Interface_des_pmodHB5_avec_l.27ATMega16
