Tracking visuel
------

NOTE: les projets et TP sont réalisés avec des [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre)!


Dans ce TP nous utiliserons la vision pour commander des moteurs.
La vision sera prise en charge par le mini-ordinateur raspberry Pi, qui fonctionne sous linux.
Les moteurs sont des servomoteurs. Leur commande sera effectuée par un arduino, comme dans le TP précédent.
La communication entre l'arduino et le raspberry pi se fera par le port série, comme dans le TP précédent.
Nous devrons nous connecter avec le raspberry pi qui sera connecté au réseau.

Rappels Python
------
A faire avec l'enseignant.


Commande de servos depuis arduino
------
Connecter 2 servos sur l'arduino. Branchement:

*    Le fil rouge est à relier au 5V de l'arduino, ou à une alimentation externe. Le fil noir est la masse commune.
*    Le fil jaune est le fil de données doit être connecté à un pin qui supporte le PWM (avec un symbole ~).


Ouvrez le logiciel Arduino IDE.

>    **Question**:  ouvrez et compilez le programme sweep, dans Fichiers/Exemples/Servo. Modifiez-le pour que les deux servos bougent.


Commande de servos depuis python via l'interface série
------
Ouvrez un terminal, et l'éditeur de texte geany.
On veut établir un dialogue série avec l'arduino, il faut donc envoyer des données sur l'interface série depuis le PC sous python, et côté arduino il faut décoder ces informations et les utiliser.

On va utiliser l'exemple suivant, où on transmet le numéro d'une LED et son niveau d'éclairement.
Les fichiers se trouvent dans le répertoire [code](https://gitlab.com/hazaa/iut-projets/tree/master/code).

*    Côté python, on envoie des informations avec le programme SerialNumericParser.py. Les informations sont la chaîne de caractères "s5,200\n". Le caractère "s" annonce le début du message. Le caractère de saut de ligne "\n" marque la fin du message. L'information utile est donc constituée de deux entiers, 5 et 20, séparés par une virgule.
*    Côté arduino, on lit les informations avec le programme SerialNumericParser.ino, et on commande la LED en fonction des informations recues. On renvoie le tout à l'envoyeur, pour vérification, avec la commande Serial.print("...") .


>    **Question**: téléchargez le programme .ino vers l'arduino. Ouvrez le moniteur série et taper "s5,200"

>    **Question**:  et lancez le programme .py sur votre PC. 

Puis, on veut transformer cet exemple pour commander deux servomoteurs, en envoyant deux angles entiers compris entre 0 et 180.

>    **Question**: modifier l'exemple ci-dessus, ainsi que la commande du servo vue plus haut pour commander les deux servos depuis le pc.


Raspberry Pi: connection
------
A faire avec l'enseignant.

Rappel: 

*    la liste des [adresses IP](http://eprel.u-pec.fr/eprel//nzltnwj/7603/document/2016_2017/co/grain_raspberry.html) des raspberry pi.
*    les commandes [linux](http://eprel.u-pec.fr/eprel//nzltnwj/7603/document/2016_2017/co/grain_linux_FAQ.html)

ATTENTION: 

*    ne jamais débrancher la raspberry sans avoir demandé à l'enseignant.
*    à l'allumage: brancher le cable réseau avant l'alimentation.

Raspberry Pi: commande de servos
------

On veut commander les servos depuis la raspberry. Le principe est exactement le même que ci-dessus.
Il faut brancher le cable USB relié à l'arduino à la raspberry, au lieu de le brancher au PC.
Puis connectez-vous sur le raspberry avec le login mctr.

>    **Question**: reprendre la question ci-dessus, et commander les servos depuis la raspberry.


Raspberry Pi: tracking visuel d'objet coloré
------

La raspberry pi est munie d'une caméra de bonne qualité (voir sa [doc](docs/README.md)).
Avec cette caméra et la librairie python OpenCV, on peut effectuer du traitement d'images et détecter des couleur ou des visages.

*    Connectez-vous sur le raspberry avec le login mctr
*    Vérifiez que [test_track_wheezy.py](https://gitlab.com/hazaa/iut-projets/blob/master/code/vision_track/test_track_wheezy.py) est accessible dans le répertoire.
*    S'il ne l'est pas, il faudra le copier à partir d'un PC de la salle de TP.
*    Demandez à l'enseignant d'activer la caméra.

>    **Question**: lancer le programme test_track_wheezy.py avec python, vérifier qu'on peut suivre le mouvement d'un objet coloré.

>    **Question**: ouvrez ce programme avec un éditeur de texte, et essayez de comprendre comment il fonctionne.


Raspberry Pi: tracking + commande de servos
------
On souhaite maintenant commander les servos à partir du tracking visuel.
On veut que l'objet coloré soit positionné au centre de l'image, qui fait 320 pixels en largeur et 200 pixels en hauteur.
Il faut donc calculer la position en pixels du centre de l'image.
Puis il faut donner des ordres aux servos pour qu'ils maintiennent l'objet coloré au centre de l'image même si celui-ci se déplace.

Commencons par envoyer des ordres à l'arduino pour voir s'il les reçoit:

*    Connectez-vous sur raspberry avec le login mctr
*    Vérifiez que [test_track_serial.py](https://gitlab.com/hazaa/iut-projets/blob/master/code/vision_track/test_track_serial.py) est accessible dans le répertoire.
*    S'il ne l'est pas, il faudra le copier à partir d'un PC de la salle de TP.


>    **Question**:  lancer le programme test_track_serial.py avec python, téléchargez un programme côté arduino qui lit le flux série et le renvoie tel quel, pour vérifier que la communication est bonne.

Maintenant qu'on a vérifié que l'arduino recevait bien les ordres, on peut modifier le programme pour effectuer le tracking visuel.

>    **Question**: modifiez test_track_serial.py pour centrer la caméra sur l'objet coloré.

>    **Question**: réaliser une vidéo du tracking d'objet.




Extensions 
------

*     modélisation du tracking comme système linéaire du 2nd ordre.
*     console serial cable: [FTDI](docs/README.md), /usr/bin/miniterm.py
*     tracking QR code avec whycon  
*     tracking couleur rapide ping-pong
*     serial dialog: [ReadBytesUntil](https://www.arduino.cc/en/Serial/ReadBytesUntil), [exemple](http://forum.arduino.cc/index.php?topic=114035.0)
