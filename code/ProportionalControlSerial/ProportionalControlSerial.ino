// inspired by AccelStepper/ProportionalControl.pde and stepper/stepper_speedControl.ino
// 
// http://www.airspayce.com/mikem/arduino/AccelStepper/ProportionalControl_8pde-example.html#a3
//
// speed is proportional to an external signal received every 40ms
// a fixed number of step

#include <AccelStepper.h>


#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver


// This defines the analog input pin for reading the control voltage
// Tested with a 10k linear pot between 5v and GND
// #define ANALOG_IN A0

int inbyte, Target=0;
int maxspeed=3000;
void setup()
{  
  /* initialize stepper */  
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
  
   /* setSpeed() not reliable above 1000 steps per second
  http://www.airspayce.com/mikem/arduino/AccelStepper/classAccelStepper.html#ae79c49ad69d5ccc9da0ee691fa4ca235
  */
  stepper.setMaxSpeed(maxspeed);
  stepper.setSpeed(Target);
  
  /* this line is for Leonardo's, it delays the serial interface
    until the terminal window is opened*/
  //while (!Serial);
     
  /*Inizializzazione monitor seriale*/
  Serial.begin(115200);          
}


void loop()
{
  
  // Read new target position
  // int analog_in = analogRead(ANALOG_IN);
  if (Serial.available()>0)
      {
       inbyte = Serial.read();
       //inbyte = Serial.parseInt(); //read(); 
       Target =map(inbyte, 0, 255, -maxspeed, maxspeed);// 2* inbyte;
       //stepper.setSpeed(Target);
      //https://www.arduino.cc/en/Serial/Println
       //Serial.println(Target, DEC);  // print as an ASCII-encoded decimal
       Serial.write(stepper.currentPosition());
      }
    
  stepper.setSpeed(Target);
 stepper.runSpeed();
  //stepper.move(100);
  //stepper.run();  

}
