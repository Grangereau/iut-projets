# -*- coding: utf-8

# Transmettre deux informations indépendantes avec un seul octet
# (par exemple pour contrôler deux moteurs)
# n1n2_bin = | n1 | n2 |
# Comme n1 et n2 sont tous les deux compris entre 0 et 15, on peut les encoder chacun sur 
# un demi-octet
# par exemple n1 = 2 = 00000010, et n2= 12 (en decimal) = 00001100  (binaire)


##############################
# ENCODAGE DE DEUX DEMI-OCTETS DANS UN SEUL OCTET
# n1_dec et n2_dec sont deux entiers compris entre 0 et 15, en base décimale
n1 = 2
n2 = 12

# on décale n1 de 4 bits vers la droite
# on obtient donc |0010|0000|
n1_shift = n1<<4

# on ajoute les deux 
# on obtient donc l'octet |n1|n2|
n = n1_shift|n2

# vérification: on doit obtenir 
# n = 00101100
print bin(n)

##############################
# DECODAGE DE L'OCTET

# je le décode en binaire ("B"=binaire)
n1_decoded= (n & 0xF0)>>4
n2_decoded= (n & 0x0F)

print "n1_decoded=",n1_decoded," n2_decoded=",n2_decoded
