// inspired by AccelStepper/ProportionalControl.pde and stepper/stepper_speedControl.ino
// 
// http://www.airspayce.com/mikem/arduino/AccelStepper/ProportionalControl_8pde-example.html#a3
//
// speed is proportional to an external signal received every 40ms
// a fixed number of step

#include <AccelStepper.h>


int count = 0;
unsigned long lastTime=0;
int sampleTime= 40;
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver


// This defines the analog input pin for reading the control voltage
// Tested with a 10k linear pot between 5v and GND
// #define ANALOG_IN A0

int Target=0;

void setup()
{  
  /* initialize stepper */  
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
  
  /*Inizializzazione monitor seriale*/
  Serial.begin(115200);
 
  /* setSpeed() not reliable above 1000 steps per second
  http://www.airspayce.com/mikem/arduino/AccelStepper/classAccelStepper.html#ae79c49ad69d5ccc9da0ee691fa4ca235
  */
  stepper.setMaxSpeed(2000);
}
void loop()
{
  
  // Read new target position
  // int analog_in = analogRead(ANALOG_IN);


    unsigned long cur_time=millis();
    
    if (Serial.available() > 0)         
    {
    inByte = Serial.read();
    Target = 2*inByte;
     // use map if the sensor reading isn't in the right interval: map(sensorReading, 0, 1023, 0, 100);
  
     stepper.setSpeed(Target);
    //https://www.arduino.cc/en/Serial/Println
    //Serial.println(Target, DEC);  // print as an ASCII-encoded decimal
	}

 

  stepper.move(10);
  stepper.run();
  

}
