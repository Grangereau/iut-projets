/*
* dialog_serial_bin.ino
* 
* Ce programme écoute ce qui arrive sur l'interface série.
* Quand il reçoit un octet, il le renvoie aussitôt. 
* 
* cf http://josephmr.com/2014/08/31/talking-to-arduino-with-python.html
* 
*/

// déclaration de variable 
int inByte = 0;         

void setup() {
    Serial.begin(9600);
    delay(50);
    // on attend que le PC nous contacte
    while (!Serial.available()) {
      delay(300);
    }
}

void loop() {
	
	if (Serial.available() > 0)	// on attend qu'un octet arrive
    {
		// on lit l'octet qui vient d'arriver
		inByte = Serial.read();
		delay(10);
        // on renvoie à l'expéditeur l'octet qui vient d'arriver
		Serial.write(inByte);
    }    
 }
