/*
 * dialog_serial_bin.ino
 * 
 * 
 * 
 * 
 * */

int inByte = 0;         // on déclare la variable

void setup() {
	// ouverture liaison série
    Serial.begin(9600);
    delay(50);
    // on attend que la liaison série soit ouverte
      while (!Serial.available()) {
      Serial.write(0x01);
      delay(300);
    }

    
    /*while (!Serial.available()) {
    }*/
}

void loop() {

	if (Serial.available() > 0)	// on attend qu'un octet arrive
    {
		// on lit l'octet qui vient d'arriver
		inByte = Serial.read();
		delay(10);
       // on renvoie à l'expéditeur l'octet qui vient d'arriver
		Serial.write(inByte);
    }    

 }
