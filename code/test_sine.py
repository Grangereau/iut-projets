# -*- coding: utf-8

import serial
import struct
import time
import numpy as np

"""
Envoi d'un entier sur le port série pour commander l'arduino
A utiliser avec le code arduino ProportionalControlSerial.ino 

Rappels: 

Hex vers decimal:
0xab -> a*16^1 + b

Decimal vers Hex en Python:
hex(128) -> '0x80'
hex(127) -> '0x7f'

"""

PORT="/dev/ttyACM1"
BAUD="115200"#"9600"
ser = serial.Serial(PORT, BAUD)
time.sleep(1)

for i in range(100):
	speed = int(255* ((1+np.sin(2*np.pi*i/10.))/2))
	# convert speed to string
	# "!"=big-endian, std. size & alignment;  "B":unsigned byte
	s=struct.pack('!B',speed)
	ser.write(s)
	# read
	r=ser.read()
	r=struct.unpack('!B',r)
	# print and wait
	print 'speed (sent)=',speed,'position (read)=',r[0]
	time.sleep(0.05)

# set speed to zero
speed=127
s=struct.pack('!B',speed)
ser.write(s)
