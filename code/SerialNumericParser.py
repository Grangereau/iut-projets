# -*- coding: utf-8

# SerialNumericParser.py
# Côté Python

import serial
import struct
import time

# j'ouvre ma connexion série
PORT="/dev/ttyACM0"
BAUD="9600"
ser = serial.Serial(PORT, BAUD)
# j'attends un peu
time.sleep(0.5)

while True:
	# on créer un message
	message = "s5,200\n"
	# on envoie la chaine de caractères sur l'interface série
	ser.write(message)
	# on lit la réponse
	reponse=ser.readline()
	print reponse
