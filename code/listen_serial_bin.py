# -*- coding: utf-8

"""
listen_serial_bin.py

Ecouter octet par octet ce qui vient sur l'interface série.
Décoder en binaire.
Afficher.
"""

import serial
import struct
import time

# j'ouvre ma connexion série
PORT="/dev/ttyACM0"
BAUD="9600"
ser = serial.Serial(PORT, BAUD)
# j'attends un peu
time.sleep(0.5)

# boucle
while True:
	# je lis un octet
	message=ser.read(1)
	# je le décode en binaire ("B"=binaire)
	r=struct.unpack("!B", message)[0]
	# je l'affiche
	print "read:", r
