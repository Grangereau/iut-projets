# -*- coding: utf-8
# https://pyserial.readthedocs.org/en/latest/
import serial
import time

pid0 = [10, 0.001, 0.001]

PORT="/dev/ttyACM0"
BAUD="9600"
ser = serial.Serial(PORT, BAUD)
time.sleep(0.5)

ser.flushInput()
time.sleep(0.5)

ser.write(str(pid0[0])+" "+str(pid0[1])+" "+str(pid0[2]))

# read will block until data arrives
#https://pyserial.readthedocs.org/en/latest/pyserial_api.html#serial.Serial.read
print "param:",ser.read()
print "param:",ser.read()
print "param:",ser.read()
if (ser.read()!='t') : raise ValueError('expecting t')

# read and append to list
n=1000
l=[]
for i in range(n):
	#struct.unpack("b", ser.read() )
	#print "send: ", ser.read()
	l.append( ser.read() )
	
print l	
