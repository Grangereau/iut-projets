#include <AccelStepper.h>
#include <RunningMedian.h>

#define LED_PIN            13

/*Definizione costanti di tempo*/
unsigned long lastTime = 0;
unsigned long lastTime2=0;
double input=0.; 
 
/*Definizione parametri PID*/
double angMin, angMax;
double Target, Actual,Sensor,Angle;
double errSum, lastErr1, lastErr2, lastErr3;
double Kp, Ki, Kd;
 
/*Tempo di campionamento*/
int sampleTime = 40;
 
/*Numero di campioni da inviare a Matlab*/
int count = 0;
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver

/*Definizione filtro mediano*/
RunningMedian mediana = RunningMedian(5);
 
 /*Funzione che effettua il settaggio dei coefficienti PID*/
void SetTuning(double kp, double ki, double kd) {
  Kp = kp;
  Ki = ki;
  Kd = kd;
}
 
/*Funzione che satura l'uscita del regolatore PID*/
void SaturateOutput(double outmin, double outmax) {
  angMin = outmin;
  angMax = outmax;
}
 
 void setup() {
  /* initialize stepper */  
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
  
  /*Inizializzazione monitor seriale*/
  Serial.begin(9600);
 
  /*Imposto il pin 10 relativo al led come output*/
  pinMode(LED_PIN,OUTPUT);
 
  /*Inizializzazione AccelStepper*/
  stepper.setMaxSpeed(1000);
  //stepper.setSpeed(3000);
  //stepper.setAcceleration(3000);
 
  /*Inizializzazione variabili del sistema*/
  Angle = 0;
  Target = 0;
 
  /*Inizializzazione PID*/
  SetTuning(10,0.001,0.001);
  SaturateOutput(-1000,1000); 

}


void loop() {
 
 	/* target position */
    unsigned long cur_time=millis();
    if(lastTime2 -cur_time >sampleTime){
	
	Target = 150.*(1.0+sin(float(cur_time)*2.0*3.14*0.0001));
	
	
	/* actual position */
	 Actual = (double)stepper.currentPosition();
 
    /* compute correction */
	double error = Target - Actual; //errore proporzionale
    errSum += error*sampleTime; //errore integrale
    double dErr = (error + lastErr1 - lastErr2 - lastErr3)/(4*sampleTime); //errore derivativo
   
    Angle = Kp*error + Ki*errSum + Kd*dErr; //valore dell'angolo
   
    /*Saturazione sull'uscita*/
    if (Angle > angMax) {
      Angle = angMax;
    }
    if (Angle < angMin) {
      Angle = angMin;
    }   
    /*Comando la rotazione del motore*/
    //stepper.moveTo(Angle);   
    stepper.setSpeed(Angle);
     stepper.runSpeed();
    //https://www.arduino.cc/en/Serial/Println
    Serial.println(Angle, DEC);  // print as an ASCII-encoded decimal
    /*Modifica le variabili per la seguente iterazione*/
    lastErr3 = lastErr2;
    lastErr2 = lastErr1;
    lastErr1 = error;
    
    lastTime2=cur_time ;
    }
   
}
