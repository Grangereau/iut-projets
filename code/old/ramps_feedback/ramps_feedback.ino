/*
 * feedback control with ramps
see handshake.ino
http://josephmr.com/2014/08/31/talking-to-arduino-with-python.html

*/

#include "AccelStepper.h"

#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38


AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver
int inByte = 0;         // incoming serial byte

void setup() {

  // ramps init
  stepper.setMaxSpeed(200);
  stepper.setAcceleration(50);

  stepper.setEnablePin(X_ENABLE_PIN);
  //stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();

  // serial init
    Serial.begin(115200);//9600
    delay(50);
    while (!Serial.available()) {
      Serial.write(0x01);
      delay(300);
    }
    // read the byte that Python will send over
    Serial.read();
}

void loop() {
    //http://www.airspayce.com/mikem/arduino/AccelStepper/Random_8pde-example.html
    if (stepper.distanceToGo() == 0)
    {
        // Random change to speed, position and acceleration
        // Make sure we dont get 0 speed or accelerations
        delay(1000);
        stepper.moveTo(rand() % 200);
        stepper.setMaxSpeed((rand() % 200) + 1);
        stepper.setAcceleration((rand() % 200) + 1);
    }
    stepper.run();
      
	//Serial.write(0x41);
	//delay(1000);

   if (Serial.available() > 0) {
    // get incoming byte:
    inByte = Serial.read();
    // read first analog input, divide by 4 to make the range 0-255:
    //firstSensor = analogRead(A0) / 4;
    // delay 10ms to let the ADC recover:
    delay(10);
   
    // send sensor values:
    Serial.write(inByte);
    //Serial.write(firstSensor);
    //Serial.write(secondSensor);
    //Serial.write(thirdSensor);
  }
  
}



