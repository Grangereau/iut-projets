#include <AccelStepper.h>
//#include <NewPing.h>
#include <Thread.h>
#include <ThreadController.h>
#include <RunningMedian.h>
 
/*Definizione pin sensore ultrasonico*/
//#define trigPin 7
//#define echoPin 6
 
/*Definizione pin motore*/
/*#define pwmA 3
#define pwmB 11
#define brakeA 8
#define brakeB 9*/

#define LED_PIN            13

/*Definizione costanti di tempo*/
unsigned long lastTime = 0;
unsigned long lastTime2=0;
double input=0.; 
 
/*Definizione parametri PID*/
double angMin, angMax;
double Setpoint,Sensor,Angle;
double errSum, lastErr1, lastErr2, lastErr3;
double Kp, Ki, Kd;
 
/*Tempo di campionamento*/
int sampleTime = 5;
 
/*Numero di campioni da inviare a Matlab*/
int count = 0;
 
/*Flag che si riferisce allo stato della simulazione
 * con '1' manda dati a Matlab e computa il PID, con '0' no */
int flag = 0;
 
/*Definizione oggetti motore e sensore*/
#define X_STEP_PIN         54
#define X_DIR_PIN          55
#define X_ENABLE_PIN       38
AccelStepper stepper(1, X_STEP_PIN, X_DIR_PIN); // 1 = Driver

//NewPing sensore(trigPin, echoPin,500);
 
/*Definizione oggetti multithreading*/
ThreadController control = ThreadController();
Thread pidThread = Thread();
Thread motorThread = Thread();
 
/*Definizione filtro mediano*/
RunningMedian mediana = RunningMedian(5);
 
/*Effettua la computazione PID*/
void Computa() {
     
    double error = Setpoint - Sensor; //errore proporzionale
    errSum += error*sampleTime; //errore integrale
    double dErr = (error + lastErr1 - lastErr2 - lastErr3)/(4*sampleTime); //errore derivativo
   
    Angle = Kp*error + Ki*errSum + Kd*dErr; //valore dell'angolo
   
    /*Saturazione sull'uscita*/
    if (Angle > angMax) {
      Angle = angMax;
    }
    if (Angle < angMin) {
      Angle = angMin;
    }
   
    /*Comando la rotazione del motore*/
    //stepper.moveTo(Angle);
    Serial.println(Angle);
    stepper.setSpeed(Angle); //!!!!!!!!!!!!!!!!!!!!!!!!!
   
    /*Modifica le variabili per la seguente iterazione*/
    lastErr3 = lastErr2;
    lastErr2 = lastErr1;
    lastErr1 = error;
}
 
/*Funzione che effettua il settaggio dei coefficienti PID*/
void SetTuning(double kp, double ki, double kd) {
  Kp = kp;
  Ki = ki;
  Kd = kd;
}
 
/*Funzione che satura l'uscita del regolatore PID*/
void SaturateOutput(double outmin, double outmax) {
  angMin = outmin;
  angMax = outmax;
}
 

/*Funzione chiamata dal Thread Controller del PID*/
void pidCallback() {
 
	/* */
    unsigned long cur_time=millis();
    if(lastTime2 -cur_time >10){
	lastTime2=cur_time ;
	Setpoint = 300.*(1.0+sin(float(lastTime2)*2.0*3.14*0.1));
	}
 
    
   input = (double)stepper.currentPosition();
   /* //double input = 0.017*sensore.ping() - 7.4;
    if (input >= 0) { //se il valore non è negativo...
      mediana.add(input);
    }
      Sensor = mediana.getMedian(); //effettua la mediana
   */
   Sensor=input;
    if (flag == 1) { //se è cominciato l'invio dei campioni
    //Serial.println(Setpoint);
      //Serial.println(Sensor); //stampo solo quando devo inviare a matlab
      count++;
      Computa(); //computa il PID
    }
}
 
/*Funzione chiamata dal Thread Controller del motore*/
void motorCallback() {
 //  stepper.run();
 stepper.runSpeed();
}
 
/*Fase di settaggio*/
void setup() {
  /**/
  stepper.setEnablePin(X_ENABLE_PIN);
  stepper.setPinsInverted(false, false, true); //invert logic of enable pin
  stepper.enableOutputs();
  
  /*Inizializzazione monitor seriale*/
  Serial.begin(9600);
 
  /*Inizializzazione pin motore*/
  /*pinMode(pwmA, OUTPUT);
  pinMode(pwmB, OUTPUT);
  pinMode(brakeA, OUTPUT);
  pinMode(brakeB, OUTPUT);
 
  digitalWrite(pwmA, HIGH);
  digitalWrite(pwmB, HIGH);
  digitalWrite(brakeA, LOW);
  digitalWrite(brakeB, LOW);*/
 
  /*Imposto il pin 10 relativo al led come output*/
  pinMode(LED_PIN,OUTPUT);
 
  /*Inizializzazione AccelStepper*/
  stepper.setMaxSpeed(10000);
  stepper.setSpeed(3000);
 // stepper.setAcceleration(3000);
 
  /*Inizializzazione variabili del sistema*/
  Angle = 0;
  Setpoint = 25;
 
  /*Inizializzazione PID*/
  SetTuning(10,0.001,0.001);
  SaturateOutput(-10000,10000);
 
  /*Inizializzazione threads*/
  pidThread.onRun(pidCallback);
  pidThread.setInterval(sampleTime);
  motorThread.onRun(motorCallback);
  motorThread.setInterval(0.05);
 
  /*Inizializzazione ThreadConrtoller*/
  control.add(&pidThread);
  control.add(&motorThread);
}
 
/*Main program*/
void loop() {
 
  control.run();
 
  /*Fine campionamento*/
  if (count == 1000) { //ricezione dei dati da Matlab completata
    flag = 0; //non stampo e non computo il PID
    digitalWrite(LED_PIN,LOW); //led spento vuol dire invio campioni non in corso
    count = 0; //riazzero
  }
 
  /*Se siamo nei pressi del punto iniziale allora posso ricominciare l'invio a Matlab*/
  //if (Sensor >=Setpoint-1 && Sensor <= Setpoint+1 && flag == 0 && count == 0) {
  if (flag == 0 && count == 0) {
    while (Serial.available() == 0); //attendi i dati da Matlab
    //https://www.arduino.cc/en/Serial/ParseFloat
    Kp = (double)Serial.parseFloat();
    Serial.println(Kp);
    Ki = (double)Serial.parseFloat();
    Serial.println(Ki);
    Kd = (double)Serial.parseFloat();
    Serial.println(Kd);
    Serial.println('t'); //invio un token per inizio simulazione
    flag = 1; //flag che indica l'invio dei dati per Arduino
    digitalWrite(LED_PIN,HIGH); //led acceso significa invio in corso
    }
}
