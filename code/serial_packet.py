# -*- coding: utf-8

"""
see $Makeblock_XY_Plotter-master/Plotter_test/Plotter_serial/UI_send.py
"""
import serial

# initialiser l'interface série
PORT = '/dev/ttyACM0'
SPEED = 9600
ser = serial.Serial( PORT, SPEED, timeout=0,stopbits=serial.STOPBITS_TWO)

# préparer les informations qu'on veut envoyer
x=1
y=2
z=3

# envoyer la trame
ser.write(chr(0xff)+chr(0xfe)+chr(x)+chr(y)+chr(z)+chr(0xfd) )

# afficher la réponse
print ser.read()
