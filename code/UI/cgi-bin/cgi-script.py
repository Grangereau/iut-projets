#!/usr/bin/python
# -*- coding: utf-8
"""
http://stackoverflow.com/questions/18226565/how-to-pass-values-to-python-from-a-jquerymobile-slider
https://fr.wikipedia.org/wiki/Common_Gateway_Interface
https://docs.python.org/2.6/library/cgihttpserver.html
"""
import cgi
form=cgi.FieldStorage()

import json

#Real code I will be running, haven't tested it yet
#import serial
#ser = serial.Serial('dev/ttyACM0', 9600)
#ser.write("%s\n" % (form["value"]))
#ser.close()

slider_id = int(form['id'].value)
slider_value = int(form['value'].value)

#Testing code
file=open('./test.txt', "w")
s = "id=%d value=%d\n"%(slider_id,slider_value)
file.write(s)
file.close()

print "Content-type: application/json"
print
print(json.JSONEncoder().encode({"status":"ok"}))
