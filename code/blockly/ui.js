Blockly.inject(
    document.getElementById('spidey-blockly'), {
        collapse: false,
        disable: false,
        path: 'vendors/blockly/', 
        trashcan: true,
        scrollbars: true,
        toolbox: document.getElementById('blockly-toolbox')
    });

function setTab() {
    'use strict';

    var currentTab;
    if(window.location.hash) {
        currentTab = window.location.hash;
    } else {
        currentTab = '#block';
    }
    $('.spidey-tab').hide();
    $('.nav-tabs li').removeClass('active');
    
    if (currentTab === '#block') {
        $('#spidey-block').show();
        $('.block-tab-btn').addClass('active');
    } else if (currentTab === '#code') {
        $('#spidey-code').show();
        Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
        Blockly.JavaScript.STATEMENT_PREFIX = null;
        var code = Blockly.JavaScript.workspaceToCode();
        $('#spidey-code-container').html(code);
        $('.code-tab-btn').addClass('active');
    } else if (currentTab === '#control') {
        $('#spidey-control').show();
        $('.control-tab-btn').addClass('active');
    } else if (currentTab === '#help') {
        $('#spidey-help').show();
        $('.help-tab-btn').addClass('active');
    } else if (currentTab === '#config') {
        $('#spidey-config').show();
        $('.config-tab-btn').addClass('active');
    } else if (currentTab === '#about') {
        $('#spidey-about').show();
        $('.about-tab-btn').addClass('active');
    }
}
setTimeout(setTab, 0);
$(window).bind('hashchange', setTab);

$('#spidey-startsim').click(function() {
    exec.stop();
    simulator.reset();
    simulator.step();
    exec.run(true);
});

$('#spidey-startrobot').click(function() {
    if (robot.isServerConnected && robot.isRobotConnected) {
        simulator.reset();
        exec.enableRobot();
        setTimeout(exec.run, 6000);
    } else {
        terminal.print('Erreur, robot déconnecté', 'red');
    }
});

$('#spidey-stopexec').click(function() {
    exec.stop();
});

$('#spidey-save-button').click(function() {
    var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
    var xml_text = Blockly.Xml.domToText(xml);
    var blob = new Blob([xml_text], {type: 'text/plain;charset=utf-8'});
    var name = $('#spidey-save-name').val();
    saveAs(blob, name + '.blocks');
});

$('#spidey-load').change(function(e) {
    var files = e.target.files;
    for (var i=0;i<files.length;i++) {
        var reader = new FileReader();
        reader.onload = function(e) {
            var xml_text = e.target.result;
            try {
                var xml = Blockly.Xml.textToDom(xml_text);
                var tree = $('category[name="Bibliothèque"]');
                tree.append(xml.childNodes);
                Blockly.updateToolbox(document.getElementById('blockly-toolbox'));
            } catch (err) {
                alert('Erreur : le fichier de blocs est invalide ('+err+')');
            }
        };
        reader.onerror = function(e) {
            alert('Erreur du chargement du fichier');
        };
        reader.readAsText(files[i]);
    }
});

$(window).bind('beforeunload', function(){
    return 'Voulez-vous quitter la page ?';
});

function checkConnection() {
    if (robot.isServerConnected) {
        $('#spidey-connection-server')
            .removeClass('label-warning')
            .addClass("label-success")
            .html('Connecté ('+robot.serverVersion+')');
    } else {
        $('#spidey-connection-server')
            .removeClass('label-success')
            .addClass("label-warning")
            .html('Déconnecté');
    }
    if (robot.isRobotConnected) {
        $('#spidey-connection-robot')
            .removeClass('label-warning')
            .addClass("label-success")
            .html('Connecté');
    } else {
        $('#spidey-connection-robot')
            .removeClass('label-success')
            .addClass("label-warning")
            .html('Déconnecté');
    }
    setTimeout(checkConnection, 1000);
}
setTimeout(checkConnection, 1000);

function handleTerminalInput(e) {
    if (!robot.isRobotConnected) {
        terminal.print('Robot déconnecté', 'red');
        $('#spidey-config-terminal').val('');
        return false;
    }

    var text = $('#spidey-config-terminal').val();
    if (text) {
        robot.command(text, function(data) {
            terminal.print(data, 'cyan');
        });
        terminal.print(text, 'cyan');
        $('#spidey-config-terminal').val('');
    }

    return false;
}
$('#spidey-config-terminal').next('button').click(handleTerminalInput);
$('#spidey-config-terminal').keypress(function (e) {
    if (e.keyCode == 13) {
        handleTerminalInput(e);
    }
});

$('#spidey-config-odoX').val(exec.odoX);
$('#spidey-config-odoY').val(exec.odoY);
$('#spidey-config-odoA').val(exec.odoA);
$('#spidey-config-odoX').change(function(e) {
    var val = parseFloat($('#spidey-config-odoX').val());
    if (!isNaN(val)) {
        exec.odoX = val;
        terminal.print('odometrieX = ' + val, 'red');
    }
});
$('#spidey-config-odoY').change(function(e) {
    var val = parseFloat($('#spidey-config-odoY').val());
    if (!isNaN(val)) {
        exec.odoY = val;
        terminal.print('odometrieY = ' + val, 'red');
    }
});
$('#spidey-config-odoA').change(function(e) {
    var val = parseFloat($('#spidey-config-odoA').val());
    if (!isNaN(val)) {
        exec.odoA = val;
        terminal.print('odometrieA = ' + val, 'red');
    }
});

var helpContainer = $('.spidey-help-content');
var helpSectionSelected = -1;
$('.spidey-help-section').click(function(e) {
    e.preventDefault();
    var index = ($(this).index())/2;
    if (index == helpSectionSelected) {
        $(helpContainer[helpSectionSelected]).hide('fast');
        $(helpContainer[helpSectionSelected])
            .prev('.spidey-help-section').removeClass('btn-warning');
        $(helpContainer[helpSectionSelected])
            .prev('.spidey-help-section').addClass('btn-danger');
        helpSectionSelected = -1;
    } else {
        $(this).next('.spidey-help-content').show('fast');
        $(this).removeClass('btn-danger');
        $(this).addClass('btn-warning');
        if (helpSectionSelected != -1) {
            $(helpContainer[helpSectionSelected]).hide('fast');
            $(helpContainer[helpSectionSelected])
                .prev('.spidey-help-section').removeClass('btn-warning');
            $(helpContainer[helpSectionSelected])
                .prev('.spidey-help-section').addClass('btn-danger');
        }
        helpSectionSelected = ($(this).index())/2;
    }

    return false;
});


