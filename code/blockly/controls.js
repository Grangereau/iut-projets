function SpideyControls() {
    'use strict';

    this.button1 = 0;
    this.button2 = 0;
    this.button3 = 0;
    this.button4 = 0;
    this.button5 = 0;
    this.button6 = 0;
    this.slider1 = 0.5;
    this.slider2 = 0.5;
    this.slider3 = 0.5;
    this.slider4 = 0.5;
    this.joypad1X = 0;
    this.joypad1Y = 0;
    this.joypad2X = 0;
    this.joypad2Y = 0;

    var objButton1 = $('#spidey-ui-button-1');
    var objButton2 = $('#spidey-ui-button-2');
    var objButton3 = $('#spidey-ui-button-3');
    var objButton4 = $('#spidey-ui-button-4');
    var objButton5 = $('#spidey-ui-button-5');
    var objButton6 = $('#spidey-ui-button-6');
    var objSlider1 = $('#spidey-ui-slider-1');
    var objSlider2 = $('#spidey-ui-slider-2');
    var objSlider3 = $('#spidey-ui-slider-3');
    var objSlider4 = $('#spidey-ui-slider-4');
    var objJoypad1 = $('#spidey-ui-joypad-1');
    var objJoypad2 = $('#spidey-ui-joypad-2');

    var self = this;

    function round(value) {
        return Math.round(value*100)/100;
    }

    objButton1.mousedown(function(e) {
        self.button1 = 1;
        objButton1.nextAll('span').html(1);
    });
    objButton1.on('mouseup mouseout', function(e) {
        self.button1 = 0;
        objButton1.nextAll('span').html(0);
    });
    objButton2.mousedown(function(e) {
        self.button2 = 1;
        objButton2.nextAll('span').html(1);
    });
    objButton2.on('mouseup mouseout', function(e) {
        self.button2 = 0;
        objButton2.nextAll('span').html(0);
    });
    objButton3.mousedown(function(e) {
        self.button3 = 1;
        objButton3.nextAll('span').html(1);
    });
    objButton3.on('mouseup mouseout', function(e) {
        self.button3 = 0;
        objButton3.nextAll('span').html(0);
    });
    objButton4.mousedown(function(e) {
        self.button4 = 1;
        objButton4.nextAll('span').html(1);
    });
    objButton4.on('mouseup mouseout', function(e) {
        self.button4 = 0;
        objButton4.nextAll('span').html(0);
    });
    objButton5.mousedown(function(e) {
        self.button5 = 1;
        objButton5.nextAll('span').html(1);
    });
    objButton5.on('mouseup mouseout', function(e) {
        self.button5 = 0;
        objButton5.nextAll('span').html(0);
    });
    objButton6.mousedown(function(e) {
        self.button6 = 1;
        objButton6.nextAll('span').html(1);
    });
    objButton6.on('mouseup mouseout', function(e) {
        self.button6 = 0;
        objButton6.nextAll('span').html(0);
    });

    objSlider1.bind("slider:ready slider:changed", function (event, data) {
        self.slider1 = round(data.value);
        objSlider1.nextAll('span').html(round(data.value));
    });
    objSlider2.bind("slider:ready slider:changed", function (event, data) {
        self.slider2 = round(data.value);
        objSlider2.nextAll('span').html(round(data.value));
    });
    objSlider3.bind("slider:ready slider:changed", function (event, data) {
        self.slider3 = round(data.value);
        objSlider3.nextAll('span').html(round(data.value));
    });
    objSlider4.bind("slider:ready slider:changed", function (event, data) {
        self.slider4 = round(data.value);
        objSlider4.nextAll('span').html(round(data.value));
    });

    joystick1.onChange = function() {
        var x = round(joystick1.valueX);
        var y = round(joystick1.valueY);
        objJoypad1.nextAll('span').html('x: '+x+' ,y: '+y);
        self.joypad1X = x;
        self.joypad1Y = y;
    };
    joystick1.onCenter = function() {
        var x = round(joystick1.valueX);
        var y = round(joystick1.valueY);
        objJoypad1.nextAll('span').html('x: '+x+' ,y: '+y);
        self.joypad1X = x;
        self.joypad1Y = y;
    };
    joystick2.onChange = function() {
        var x = round(joystick2.valueX);
        var y = round(joystick2.valueY);
        objJoypad2.nextAll('span').html('x: '+x+' ,y: '+y);
        self.joypad2X = x;
        self.joypad2Y = y;
    };
    joystick2.onCenter = function() {
        var x = round(joystick2.valueX);
        var y = round(joystick2.valueY);
        objJoypad2.nextAll('span').html('x: '+x+' ,y: '+y);
        self.joypad2X = x;
        self.joypad2Y = y;
    };
}

var controls = new SpideyControls();

