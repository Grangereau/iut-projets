function Joystick(containerId) {
    'use strict';

    function pauseEvent(e){
        e.stopPropagation();
        e.preventDefault();
        e.cancelBubble=true;
        e.returnValue=false;
        return false;
    }

    var radius = 70;
    var size = 30;
    var containerSize = 200;

    this.valueX = 0;
    this.valueY = 0;
    var self = this;

    var container = $('#' + containerId);
    container.append('<div class="joypad-stick"></div>');
    container.append('<div class="joypad-center"></div>');
    
    var center = container.children('.joypad-center');
    center.css('width', size/4+'px');
    center.css('height', size/4+'px');
    center.css('display', 'none');
    center.css('position', 'relative');
    center.css('left', containerSize/2-size/8+'px');
    center.css('top', containerSize/2-size/8+'px');
    
    var stick = container.children('.joypad-stick');
    stick.css('width', size+'px');
    stick.css('height', size+'px');
    stick.css('position', 'relative');
    stick.css('left', containerSize/2-size/2+'px');
    stick.css('top', containerSize/2-size/2+'px');
    stick.css('cursor', 'pointer');

    this.onChange = function() {};
    this.onCenter = function() {};

    stick.mousedown(function(e) {
        pauseEvent(e);
        var lastMove = 0;
        var initX = e.pageX;
        var initY = e.pageY;
        stick.css('cursor', 'move');
        stick.css('position', 'absolute');
        stick.css('left', initX-size/2);
        stick.css('top', initY-size/2);
        center.css('display', 'block');
        $(document).mousemove(function(e) {
            pauseEvent(e);
            var now = Date.now();
            if (now > lastMove + 50) {
                lastMove = now;
                var x = e.pageX;
                var y = e.pageY;
                var len = Math.sqrt(
                    (x-initX)*(x-initX)+
                    (y-initY)*(y-initY));
                if (len > radius) {
                    x = radius*(x-initX)/len + initX;
                    y = radius*(y-initY)/len + initY;
                }
                self.valueX = -(y - initY)/radius;
                self.valueY = -(x - initX)/radius;
                x -= size/2;
                y -= size/2;
                stick.css('left', x);
                stick.css('top', y);
                self.onChange();
            }
        });
        $(document).mouseup(function() {
            pauseEvent(e);
            stick.css('cursor', 'pointer');
            center.css('display', 'none');
            $(document).off('mousemove');
            $(document).unbind('mousemove');
            $(document).off('mouseup');
            $(document).unbind('mouseup');
            stick.css('position', 'relative');
            stick.css('left', containerSize/2-size/2+'px');
            stick.css('top', containerSize/2-size/2+'px');
            self.valueX = 0;
            self.valueY = 0;
            self.onCenter();
        });
    });
}

var joystick1 = new Joystick('spidey-ui-joypad-1');
var joystick2 = new Joystick('spidey-ui-joypad-2');

