function SpideyTerminal() {
    'use strict';

    var container = $('#spidey-terminal');
    var self = this;

    this.print = function(line, color) {
        var code = '&raquo; <span style="color: ';
        if (color == undefined) {
            code += 'white';
        } else {
            code += color;
        }
        var line = new String(line);
        line = line.replace(/(?:\r\n|\r|\n)/g, '<br />');
        code += ';">' + line + '</span><br/>';
        container.append(code);

        var height = container[0].scrollHeight;
        container.scrollTop(height);
    };
    
    this.reset = function() {
        container.html('');
        self.print('Metabot Terminal', 'red');
    };

    this.reset();
}

var terminal = new SpideyTerminal();

