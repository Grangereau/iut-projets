Blockly.Blocks['zmq_subscribe'] = {
  init: function() {
	  this.appendDummyInput()
        .appendField("socket subscribe");
    this.appendValueInput("IP")
        .setCheck("String")
        .appendField("IP");
    this.appendValueInput("PORT")
        .setCheck("Number")
        .appendField("PORT");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['zmq_subscribe'] = function(block) {
  //var value_ip =  block.getFieldValue('IP')	;
  var value_ip = Blockly.Python.valueToCode(block, 'IP', Blockly.Python.ORDER_ATOMIC);
  var value_port = Blockly.Python.valueToCode(block, 'PORT', Blockly.Python.ORDER_ATOMIC);  
  //var code = "context = zmq.Context() \n socket = context.socket(zmq.SUB) \n socket.connect ('tcp://"+value_ip.toString()+" :"+ value_port+"' ) \n" ;
  var code = "import zmq\ncontext = zmq.Context() \nsocket = context.socket(zmq.SUB) \nsocket.connect ('tcp://%s:%s' %("+ value_ip + ", "+ value_port+"))\nsocket.setsockopt(zmq.SUBSCRIBE, '')\n" ;
  //var code = "value_seconds = \"" + value_ip.toString() + "\"\n";
  return code;
};


Blockly.Blocks['zmq_recv'] = {
  init: function() {
	this.appendDummyInput()
        .appendField("socket recv");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
    this.setTooltip('');
    this.setHelpUrl('http://www.example.com/');
  }
};

Blockly.Python['zmq_recv'] = function(block) {
  // TODO: Assemble Python into code variable.
  var code = '...\n';
  var code = 'msg=socket.recv();\n';
  return code;
};
