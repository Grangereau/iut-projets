function SpideyRobot(remoteUrl) {
    'use strict'; 

    var self = this;
    var remote = remoteUrl;

    this.isServerConnected = false;
    this.isRobotConnected = false;
    this.serverVersion = '';
    this.robotVersion = '';

    function invoke(command, argument, callback) {
        if (typeof(argument) == 'undefined') {
            argument = null;
        }
        $.ajax({
            url: remote + '/?' + $.toJSON([command, argument]),
            dataType: 'jsonp',
            success: function(data) {  
                if (typeof(callback) != 'undefined' && data[0]) {
                    callback(data[1]);
                }
            },
            error: function() {
                self.isServerConnected = false;
                self.isRobotConnected = false;
            },
        });
    }

    this.checkPort = function(callback) {
        invoke('connected', null, callback);
    };

    this.getVersion = function(callback) {
        invoke('version', null, callback);
    };

    this.command = function(command, callback) {
        invoke('command', command, callback);
    };

    function checkConnection() {
        self.getVersion(function(version) {
            self.isServerConnected = true;
            self.serverVersion = version;
        });
        self.checkPort(function(val) {
            if (val) {
                self.isRobotConnected = true;
            } else {
                self.isRobotConnected = false;
            }
        });
        setTimeout(checkConnection, 1000);
    }

    setTimeout(checkConnection, 1000);
}

var robot = new SpideyRobot('http://localhost:5757');

