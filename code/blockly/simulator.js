function SpideySimulator() {
    'use strict';

    var canvas = document.getElementById('spidey-canvas');
    var ctx = canvas.getContext('2d');
    var spideyImg = new Image();
    spideyImg.src = 'spidey.png';

    var spideyX;
    var spideyY;
    var spideyAngle;
    var spideyPath = [];
    var spideyVelX;
    var spideyVelY;
    var spideyVelAngle;
    var spideyPathColor;
    var spideyIsWriting;
    var zoom = 1.0;

    var period = 200;

    var self = this;
    this.odoX = 1.0;
    this.odoY = 1.0;
    this.odoA = 1.0;

    this.drawSpidey = function(x, y, angle, color) {
        var spideyScale = 0.8;
        ctx.save();
        ctx.translate(x, y);
        ctx.rotate(angle);
       
        ctx.lineWidth = 5;
        ctx.strokeStyle = color;
        ctx.lineCap = 'round';

        ctx.beginPath();
        ctx.arc(0, 0, 
            0.5*spideyImg.height*spideyScale, 
            0.9+Math.PI/2.0, 
            2.0*Math.PI-0.9+Math.PI/2.0);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.lineTo(0, 40);
        ctx.moveTo(0, 40);
        ctx.lineTo(10, 25);
        ctx.moveTo(0, 40);
        ctx.lineTo(-10, 25);
        ctx.stroke();

        ctx.drawImage(spideyImg, 
            -spideyImg.width*spideyScale/2.0, 
            -spideyImg.height*spideyScale/2.0, 
            spideyImg.width*spideyScale, 
            spideyImg.height*spideyScale);
        ctx.restore();
    };

    this.drawPath = function() {
        ctx.save();
        ctx.lineWidth = 4;
        ctx.lineCap = 'round';
        for (var i=1;i<spideyPath.length;i++) {
            if (spideyPath[i-1].isWriting && spideyPath[i].isWriting) {
                ctx.beginPath();
                ctx.moveTo(spideyPath[i-1].x, spideyPath[i-1].y);
                ctx.lineTo(spideyPath[i].x, spideyPath[i].y);
                ctx.strokeStyle = spideyPath[i].color;
                ctx.stroke();
            }
        }
        ctx.restore();
    }

    this.draw = function() {
        ctx.clearRect(0, 0, 500, 500);
        ctx.save();
        ctx.translate(250, 250);
        ctx.scale(zoom, zoom);
        ctx.translate(-250, -250);
        self.drawPath();
        self.drawSpidey(
            spideyX, spideyY, 
            spideyAngle, 
            '#F40000');
        ctx.restore();
    };

    this.reset = function() {
        spideyX = 250;
        spideyY = 250;
        spideyAngle = 0;
        spideyPath = [];
        spideyVelX = 0.0;
        spideyVelY = 0.0;
        spideyVelAngle = 0.0;
        spideyPathColor = 'black';
        spideyIsWriting = true;
        zoom = 1.0;
        spideyPath.push({
            x: spideyX, 
            y: spideyY,
            color: spideyPathColor,
            isWriting: spideyIsWriting
        });
    };

    this.stop = function() {
        spideyVelX = 0.0;
        spideyVelY = 0.0;
        spideyVelAngle = 0.0;
    };

    this.move = function(dx, dy) {
        spideyX -= dy*Math.cos(spideyAngle) + dx*Math.sin(spideyAngle);
        spideyY -= dy*Math.sin(spideyAngle) - dx*Math.cos(spideyAngle);
        spideyPath.push({
            x: spideyX, 
            y: spideyY,
            color: spideyPathColor,
            isWriting: spideyIsWriting
        });
    };
    
    this.rotate = function(da) {
        spideyAngle += -da*Math.PI/180.0;
    };

    this.setVelMove = function(velX, velY) {
        spideyVelX = velX;
        spideyVelY = velY;
    };
    this.setVelRotate = function(velRotate) {
        spideyVelAngle = velRotate;
    };

    this.setPathColor = function(color) {
        spideyPathColor = color;
    };

    this.setWriting = function (bool) {
        spideyIsWriting = bool;
    };

    this.step = function() {
        self.rotate(period*spideyVelAngle/1000.0);
        self.move(period*spideyVelX/1000.0, period*spideyVelY/1000.0);
        var dist = Math.sqrt(
            (spideyX-250)*(spideyX-250) +
            (spideyY-250)*(spideyY-250));
        if (dist > 250/zoom) {
            zoom /= 2.0;
        }
        self.draw();
    };

    this.run = function(p) {
        period = (p == undefined ? period : p);
        setInterval(self.step, period);
    };

    this.reset();
}

var simulator = new SpideySimulator();
simulator.step();

