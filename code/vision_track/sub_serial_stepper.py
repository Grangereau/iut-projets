# -*- coding: utf-8

"""
sub_serial_stepper.y

ce programme écoute ce qui vient de la raspberry, c-est-à-dire
la position (x,y) en pixel

il en déduit une commande déplacement pour les moteurs, sous forme
d'une octet envoyé sur le port série

Attention: côté arduino, il faut flasher un programme qui 
lise un octet et commande la position 
"""

import zmq
import serial
import struct
import time


# camera: configuration de l'image
width = 320
height = 240
# la camera est branchée sur le raspberry pi.
# il faut indiquer la bonne adresse IP de votre raspberry
ip="10.14.72.62"  
port = "5556"      # ne pas modifier
topicfilter=""

# connexion à la socket de la raspberry
context = zmq.Context()
socket = context.socket(zmq.SUB)
socket.connect ("tcp://%s:%s" %(ip, port))
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

# connexion série vers l'arduino
PORT="/dev/ttyACM0"
BAUD="9600"
ser = serial.Serial(PORT, BAUD)

def compute_speed(pos,maxpos):
	"""
	compute speed from current position
	return a byte
	"""
	# compute speed
	# posX is an integer in [0,width]
	# speed must be in [0,255]
	a=float(255)/float(width)
	speed = posX*a	
	# convert speed to string
	# "!"=big-endian, std. size & alignment;  "B":unsigned byte
	s=struct.pack('!B',speed)
	return s


# boucle
while True:
	# lit ce qui vient du socket
	string = socket.recv()
	posX, posY = string.split()
	posX=int(posX)
	posY=int(posY)
	print "raspberry: posX=", posX, " posY=",posY
	# calcule la vitesse à imposer au moteur
	s = compute_speed(posX,width)
	# idem Y
	# envoyer la consigne à l'arduino sur l'interface série
	ser.write(s)
	# read position
	r=ser.read()
	r=struct.unpack('!B',r)
	# print
	print 'PC: speed (sent)=',speed,'position (read)=',r[0]# compute speed



