# -*- coding: utf-8
# Raspbery Pi Color Tracking Project

# cv: http://blog.oscarliang.net/raspberry-pi-color-tracking-opencv-pid/
#     http://docs.opencv.org/3.0.0/df/d9d/tutorial_py_colorspaces.html
# cv2: http://stackoverflow.com/questions/22588146/tracking-white-color-using-python-opencv

import cv2
import numpy as np

# http://docs.opencv.org/2.4.11/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-set
w=320
h=200
cap = cv2.VideoCapture(0)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, w)
cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, h)

while(1):
	_, frame = cap.read()
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # define range of white color in HSV
    # change it according to your need !
	lower_white = np.array([0,0,0], dtype=np.uint8)
	upper_white = np.array([0,0,255], dtype=np.uint8)
	low = np.array([110,50,50])
	hi = np.array([130,255,255])
    # Threshold the HSV image to get only white colors
	mask = cv2.inRange(hsv, low, hi)
    # Bitwise-AND mask and original image
	res = cv2.bitwise_and(frame,frame, mask= mask)

	# moments
	moments = cv2.moments(mask, 0)
	#print moments
	area = moments['m00']
	if(area > 60000):
	# Calculating the center postition of the blob
		posX = int(moments['m10'] / area)
		posY = int(moments['m01']/ area)
		print posX,posY
		cv2.circle(frame, (posX,posY),10, (0,255,0),10 )
	# show image
	cv2.imshow('frame',frame)
	cv2.imshow('mask',mask)
	cv2.imshow('res',res)

	k = cv2.waitKey(5) & 0xFF
	if k == 27:
		break

cv2.destroyAllWindows()
