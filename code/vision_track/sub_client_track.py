# -*- coding: utf-8
"""
Lecture de la position du point coloré sur le réseau
en se connectant à la raspberry
"""

import zmq

# la camera est branchée sur le raspberry pi.
# il faut indiquer la bonne adresse IP de votre raspberry
#ip="10.14.72.62"  
ip="localhost"
port = "5556"      # ne pas modifier
topicfilter=""
print "listening ip=",ip,":",port, "topic=",topicfilter

# connexion à la socket de la raspberry
context = zmq.Context()
socket = context.socket(zmq.SUB)

socket.connect ("tcp://%s:%s" %(ip, port))
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

# boucle
while True:
    string = socket.recv()
    try:
		posX, posY = string.split()
		posX=int(posX)
		posY=int(posY)
		print "posX=", posX, " posY=",posY
    except ValueError as e:
		print "string=",string
